namespace Clients;

public class Client
{

    public string CIN { get; private set; }
    public string Nom { get; private set; }
    public string Prenom { get; private set; }
    public string Tel { get; private set; } 

    public Client(string cin, string nom, string prenom, string tel)
    {
        CIN = cin;
        Nom = nom;
        Prenom = prenom;
        Tel = tel;
    }
    public string Afficher()
    {
        return $@"
        CIN:{CIN}
        Nom:{Nom}
        Prénom:{Prenom}
        Tél:{Tel}";
    }
}