using System;
using Clients;

namespace Comptes;

public class Compte
{
    public double Solde { get; set; }
    public int Code { get; set; }
    public Client Proprietaire { get;}
    public static int compteur = 0; 

    public Compte(Client proprietaire, double soldeDepart = 0)
    {
        Proprietaire = proprietaire; 
        Solde = soldeDepart;
        Code = compteur++;
     
    }
    public string NombreCompte()
    {
        return $"Le nombre de comptes crées: {compteur}";
    }

    public void Debiter(double money)
    {
        Solde -= money;
    }

    public void Crediter(double somme)
    {
        Solde += somme;
    }

    public void Crediter(double argent, Compte destinataire)
    {
        Crediter(argent);
        destinataire.Debiter(argent);
    }

    public void Debiter(double somme, Compte compteBancaire)
    {
        Debiter(somme);
        compteBancaire.Crediter(somme);
    }

    public string Resumer()
    {
        return $@"
        ***********************
        Numéro de Compte: {Code}
        Solde de compte: {Solde} ar
        Propriétaire du compte:
        {Proprietaire.Afficher()}
        ************************";
    }
}
