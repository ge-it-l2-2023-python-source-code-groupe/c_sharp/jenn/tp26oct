﻿using Comptes;
using Clients;

namespace TP23octobre;
public class Program
{
    static void Main(string[] args)
    {
        string cin, nom, prenom, tel;


        Console.WriteLine(" Compte n°1:\n Donner le CIN: ");
        cin = Console.ReadLine() ?? " ";
        Console.WriteLine("Donner le Nom: ");
        nom = Console.ReadLine() ?? " ";
        Console.WriteLine("Donner le Prénom: ");
        prenom = Console.ReadLine()?? " ";
        Console.WriteLine("Donner le numéro de téléphone ");
        tel = Console.ReadLine() ?? " ";

        Compte compte1 = new Compte(new Client(cin, nom, prenom, tel));
        Console.WriteLine($"Détails du compte:{compte1.Resumer()}");

        //Crediter compte1  
        Console.WriteLine("Donner le montant à déposer : ");
        double somme = Convert.ToDouble(Console.ReadLine());
        compte1.Crediter(somme);
        Console.WriteLine($"Opération bien effectuée{compte1.Resumer()}");

        //Debiter compte1
        Console.WriteLine(" Compte n°2:\n Donner le CIN: ");
        cin = Console.ReadLine() ?? " ";
        Console.WriteLine("Donner le Nom: ");
        nom = Console.ReadLine() ?? " ";
        Console.WriteLine("Donner le Prénom: ");
        prenom = Console.ReadLine() ?? " ";
        Console.WriteLine("Donner le numéro de téléphone ");
        tel = Console.ReadLine()?? " ";

        Compte compte2 = new Compte(new Client(cin, nom, prenom, tel));
        Console.WriteLine($"Détails du compte:{compte1.Resumer()}");

        //Crediter compte2
        Console.WriteLine($"Crediter le compte {compte2.Code} à partir du compte {compte1.Code}");
        Console.Write($"Donner le montant à déposer: ");
        somme = Convert.ToDouble(Console.ReadLine());
        compte2.Crediter(somme, compte1);
        Console.WriteLine($"Opération bien effectuée");

        //Debiter compte1
        Console.WriteLine($"Débiter le compte {compte1.Code} et créditer le compte {compte2.Code}");
        Console.Write($"Donner le montant à retirer: ");
        somme = Convert.ToDouble(Console.ReadLine());
        compte1.Debiter(somme, compte2);
        Console.Write($"Opération bien effectuée");

        Console.WriteLine($"{compte1.Resumer()}");
        Console.WriteLine($"{compte1.Resumer()} {compte2.Resumer()} ");

        Console.WriteLine($"{compte2.NombreCompte()}");
        Console.ReadKey();
    }
}

